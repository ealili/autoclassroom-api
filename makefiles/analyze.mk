### FORMAT
# ¯¯¯¯¯¯¯¯

analyze.mypy: ## Run black on every file
	docker-compose run --rm server bash -c "python -m mypy"