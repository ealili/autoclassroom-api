"""
Defines the blueprint for the csv uploader
"""
from flask import Blueprint
from flask_restful import Api

from resources import CSVResource

CSV_BLUEPRINT = Blueprint("csv", __name__)
Api(CSV_BLUEPRINT).add_resource(
    CSVResource, "/csv"
)
