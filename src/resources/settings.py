"""
Define the REST verbs relative to the settings
"""

from flask import request
from flask.json import jsonify
from flask_restful import Resource

from decorators import token_required
from repositories import SettingsRepository


class SettingsResource(Resource):
    """ Verbs relative to the settings """

    @staticmethod
    @token_required
    def get(user, _id):
        """ Return users settings based on his/her id"""
        settings = SettingsRepository.get(id=_id)
        return jsonify({"settings": settings.json})

    @staticmethod
    @token_required
    def post(_id):
        # """ Create an user based on the sent information """
        # settings = SettingsRepository.create(api_key=api_key,
        # designated_admin=designated_admin, domain=domain,
        # gsuite_id=gsuite_id, organization_name=organization_name)
        # return jsonify({"settings": settings.json})

        """ Update an user based on the sent information """
        repository = SettingsRepository()
        api_key = request.form['api_key']
        designated_admin = request.form['designated_admin']
        organization_name = request.form['organization_name']
        settings = repository.update(id=_id, api_key=api_key, designated_admin=designated_admin,
                                     organization_name=organization_name)

        return jsonify({"settings": settings.json})
