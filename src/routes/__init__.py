from .classroom import CLASSROOM_BLUEPRINT
from .csv import CSV_BLUEPRINT
from .insights import INSIGHTS_BLUEPRINT
from .role import ROLE_BLUEPRINT
from .settings import ALLSETTINGS_BLUEPRINT, SETTINGS_BLUEPRINT
from .user import USER_BLUEPRINT, USERS_BLUEPRINT
from .verification import VERIFICATION_BLUEPRINT
