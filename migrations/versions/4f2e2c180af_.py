"""Initial migration

Revision ID: 4f2e2c180af
Revises: None
Create Date: 2019-12-30 18:44:11.102295

"""
from alembic import op
import sqlalchemy as sa
import datetime

revision = '4f2e2c180af'
down_revision = None


def upgrade():
    op.create_table('classroom',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('classroom_id', sa.String(length=300), nullable=False),
                    sa.Column('teachers', sa.String(length=300), nullable=False),
                    sa.Column('school', sa.String(length=300), nullable=False),
                    sa.Column('school_count', sa.Integer(), default=0),
                    sa.Column('course_name', sa.String(length=300), nullable=False),
                    sa.Column('section', sa.String(length=300), nullable=False),
                    sa.Column('course_state', sa.String(length=20), nullable=False),
                    sa.Column('guardians_enabled', sa.BOOLEAN()),
                    sa.Column('primary_teacher', sa.String(length=300), nullable=False),
                    sa.Column('primary_teacher_id', sa.Integer(), nullable=False),
                    sa.Column('primary_teacher_profile', sa.String(length=300), nullable=False),
                    sa.Column('teacher_count', sa.Integer(), nullable=True),
                    sa.Column('teachers', sa.String(length=300), nullable=True),
                    sa.Column('go_to_classroom', sa.String(length=300), default=None),
                    sa.Column('student_count', sa.Integer(), default=0),
                    sa.Column('coursework_count', sa.Integer(), default=0),
                    sa.Column('most_recent_coursework', sa.String(length=300), default=None),
                    sa.Column('announcement_count', sa.Integer(), default=0),
                    sa.Column('most_recent_announcement', sa.String(length=300), default=None),
                    sa.Column('join_code', sa.String(length=300), nullable=False),
                    sa.Column('last_refresh', sa.DateTime(), default=datetime.datetime.utcnow()),
                    sa.Column('most_recent_activity', sa.String(length=300), default=None),
                    sa.Column('created_at', sa.DateTime(), default=datetime.datetime.utcnow()),
                    sa.Column('updated_at', sa.DateTime(), default=datetime.datetime.utcnow()),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('organization_type',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=50), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('teacher',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=50), nullable=False),
                    sa.Column('username', sa.String(length=300), nullable=False),
                    sa.Column('email', sa.String(length=300), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('classroom_teacher',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('classroom_id', sa.Integer(), nullable=True),
                    sa.Column('teacher_id', sa.Integer(), nullable=True),
                    sa.Column('is_primary', sa.Boolean(), nullable=True),
                    sa.Column('created_at', sa.DateTime(), nullable=True),
                    sa.Column('updated_at', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['classroom_id'], ['classroom.id'], ),
                    sa.ForeignKeyConstraint(['teacher_id'], ['teacher.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('organization_unit',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=50), nullable=False),
                    sa.Column('organization_type_id', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['organization_type_id'], ['organization_type.id']),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('teacher_organization_unit',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('organization_unit_id', sa.Integer(), nullable=True),
                    sa.Column('teacher_id', sa.Integer(), nullable=True),
                    sa.Column('created_at', sa.DateTime(), nullable=True),
                    sa.Column('updated_at', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['organization_unit_id'], ['organization_unit.id']),
                    sa.ForeignKeyConstraint(['teacher_id'], ['teacher.id']),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('invitation',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('email', sa.String(300), nullable=False),
                    sa.Column('role', sa.String(300), nullable=False),
                    )

    op.create_table('role',
                    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
                    sa.Column('name', sa.String(300), nullable=False),
                    sa.Column('description', sa.String(600), nullable=True, default=None),
                    sa.Column('read_only', sa.Boolean(), default=False, nullable=False)
                    )

    op.create_table('user',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('first_name', sa.String(length=300), nullable=False),
                    sa.Column('last_name', sa.String(length=300), nullable=False),
                    sa.Column('email', sa.String(length=300), nullable=False),
                    sa.Column('role_id', sa.Integer(), nullable=False),
                    sa.Column('suspended', sa.Boolean(), default=False),
                    sa.Column('last_access', sa.DateTime(), nullable=True),
                    sa.Column('picture_url', sa.String(length=500), nullable=True),
                    sa.ForeignKeyConstraint(['role_id'], ['role.id']),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('settings',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('organization_name', sa.String(length=300), nullable=False),
                    sa.Column('domain', sa.String(length=300), nullable=False),
                    sa.Column('gsuite_id', sa.String(length=300), nullable=False),
                    sa.Column('designated_admin', sa.Integer(), nullable=False),
                    sa.Column('api_key', sa.String(length=300), nullable=False),
                    sa.ForeignKeyConstraint(['designated_admin'], ['user.id']),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.create_table('student',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=50), nullable=False),
                    sa.Column('email', sa.String(length=300), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )


def downgrade():
    op.drop_table('classroom_teacher')
    op.drop_table('classroom')
    op.drop_table('teacher_organization_unit')
    op.drop_table('teacher')
    op.drop_table('organization_unit')
    op.drop_table('organization_type')
    op.drop_table('settings')
    op.drop_table('user')
    op.drop_table('role')
    op.drop_table('student')
    op.drop_table('invitation')
