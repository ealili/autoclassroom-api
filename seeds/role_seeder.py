from seeds.base_seeder import BaseSeeder
from models import Role


class RoleSeeder(BaseSeeder):
    def run(self):
        role_1 = Role("Default Reporter", "", True)
        role_2 = Role("District Level Operations Admin", "", False)
        role_3 = Role("Instructional Coach", "", False)
        role_4 = Role("Instructional Supervisor", "", False)
        role_5 = Role("School Level Operations Admin", "", False)
        role_6 = Role("Super Admin",
                      "A user with this role can perform all functions within Little SIS", True)
        role_7 = Role("Teacher", "", False)

        self.db.session.add(role_1)
        self.db.session.add(role_2)
        self.db.session.add(role_3)
        self.db.session.add(role_4)
        self.db.session.add(role_5)
        self.db.session.add(role_6)
        self.db.session.add(role_7)
