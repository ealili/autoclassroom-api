"""
Define the Student model
"""

from . import db
from .abc import BaseModel, MetaBaseModel


class Student(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Student model """

    __tablename__ = "student"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(300), nullable=False)

    def __init__(self, name, email):
        """ Create a new Student """
        self.name = name
        self.email = email
