from seeds.base_seeder import BaseSeeder
from models import Role, User
from datetime import datetime


class UserSeeder(BaseSeeder):
    def run(self):
        role_id = Role.query.filter_by(name='Super Admin').first().id
        users = [User('Agni', 'Ramadani', 'ag27081@seeu.edu.mk', role_id, False, datetime.now(),
                      'https://lh3.googleusercontent.com/a-/AOh14GhDHeFHTE4JeusX1TGr3-oDJCtLTccx'
                      'JqBTp2Q0=s70-p-k-rw-no'),
                 User('Selaudin', 'Agolli', 'sa27036@seeu.edu.mk', role_id, False, datetime.now(),
                      'https://lh3.googleusercontent.com/a-/AOh14GinBOKEzsgO4ntIj4D12n21n8OOZVe7'
                      'gmCg7jfF=s70-p-k-rw-no'),
                 User('Enes', 'Alili', 'ea27061@seeu.edu.mk', role_id, False, datetime.now(),
                      'https://lh3.googleusercontent.com/a-/AOh14GjRgCHtwQTgEe5rt_4xTmhDcHQlEnGb'
                      'SUZ8rDbqYg=s70-p-k-rw-no'),
                 User('Google', 'Admin', 'googleadmin@wilsonschool.edu.mk', role_id, False,
                      datetime.now(),
                      'https://lh3.googleusercontent.com/-LRP5LYD-pbc/AAAAAAAAAAI/AAAAAAAAAAA/AMZ'
                      'uuckL7gbR9izNE_7N69RPLnErmreDXA/s96-c-rg-br100/photo.jpg')]

        for user in users:
            self.db.session.add(user)
