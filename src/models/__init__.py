from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()  # isort:skip

from .classroom import Classroom
from .organization_unit import OrganizationUnit
from .teacher import Teacher
from .teacher_organization_unit import TeacherOrganizationUnit
from .classroomteacher import ClassroomTeacher
from .organization_type import OrganizationType
from .role import Role
from .user import User
from .settings import Settings
from .student import Student
from .course import Course
from .invitation import Invitation
