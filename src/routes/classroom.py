"""
Defines the blueprint for the classrooms
"""
from flask import Blueprint
from flask_restful import Api

from resources import ClassroomResource

CLASSROOM_BLUEPRINT = Blueprint("classroom", __name__)
Api(CLASSROOM_BLUEPRINT).add_resource(
    ClassroomResource, "/classrooms"
)
