"""
Define the User model
"""
from sqlalchemy import ForeignKey

from . import db
from .abc import BaseModel, MetaBaseModel


class User(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The User model """

    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(300), nullable=False)
    last_name = db.Column(db.String(300), nullable=False)
    email = db.Column(db.String(300), nullable=False)
    role_id = db.Column(db.Integer, ForeignKey('role.id'))
    suspended = db.Column(db.Boolean, default=False)
    last_access = db.Column(db.DateTime, nullable=True)
    picture_url = db.Column(db.String(500), nullable=True)

    def __init__(self, first_name, last_name, email, role_id,
                 suspended, last_access, picture_url):
        """ Create a new User """
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.role_id = role_id
        self.suspended = suspended
        self.last_access = last_access
        self.picture_url = picture_url
