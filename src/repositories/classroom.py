""" Defines the Classroom repository """
from models import Classroom


class ClassroomRepository:
    """ The repository for the Classroom model """

    def __init__(self):
        pass

    @staticmethod
    def get_all():
        classrooms = Classroom.query.all()
        return classrooms
