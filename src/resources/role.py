"""
Define the REST verbs relative to the roles
"""

from flask.json import jsonify
from flask_restful import Resource

from decorators import token_required
from repositories import RoleRepository


class RoleResource(Resource):
    """ Verbs relative to the roles """

    @staticmethod
    @token_required
    def get(user):
        roles = [role.json for role in RoleRepository.get_all()]
        return jsonify({'roles': roles})
