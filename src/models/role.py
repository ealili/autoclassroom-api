"""
Define the Role model
"""

from . import db
from .abc import BaseModel, MetaBaseModel


class Role(db.Model, BaseModel, metaclass=MetaBaseModel):  # type: ignore
    """ The Role model """

    __tablename__ = "role"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(300), nullable=False)
    description = db.Column(db.String(600), nullable=True, default=None)
    read_only = db.Column(db.Boolean(), default=False, nullable=False)

    def __init__(self, name, description, read_only):
        """ Create a new Role """
        self.name = name
        self.description = description
        self.read_only = read_only
