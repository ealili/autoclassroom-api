"""
Define the Classroom model
"""

import datetime

from . import db
from .abc import BaseModel, MetaBaseModel


class Classroom(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Classroom model """

    __tablename__ = "classroom"

    id = db.Column(db.Integer, primary_key=True)
    classroom_id = db.Column(db.String(300), nullable=False)
    school = db.Column(db.String(300), nullable=False)
    school_count = db.Column(db.Integer, default=0)
    course_name = db.Column(db.String(300), nullable=False)
    section = db.Column(db.String(300), nullable=False)
    course_state = db.Column(db.String(20), nullable=False)
    guardians_enabled = db.Column(db.BOOLEAN)
    primary_teacher = db.Column(db.String(300), nullable=False)
    primary_teacher_id = db.Column(db.Integer, nullable=False)
    primary_teacher_profile = db.Column(db.String(300), nullable=False)
    teacher_count = db.Column(db.Integer, nullable=True)
    teachers = db.Column(db.String(300), nullable=True)
    go_to_classroom = db.Column(db.String(300), default=None)
    student_count = db.Column(db.Integer, default=0)
    coursework_count = db.Column(db.Integer, default=0)
    most_recent_coursework = db.Column(db.String(300), default=None)
    announcement_count = db.Column(db.Integer, default=0)
    most_recent_announcement = db.Column(db.String(300), default=None)
    join_code = db.Column(db.String(300), nullable=False)
    last_refresh = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    most_recent_activity = db.Column(db.String(300), default=None)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    updated_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())

    def __init__(self, classroom_id, school, school_count, course_name, section,
                 course_state, guardians_enabled, primary_teacher, primary_teacher_id,
                 primary_teacher_profile, teacher_count, teachers, go_to_classroom, student_count,
                 coursework_count, most_recent_coursework, announcement_count,
                 most_recent_announcement, join_code, last_refresh, most_recent_activity,
                 created_at, updated_at):
        """ Create a new Classroom """
        self.classroom_id = classroom_id
        self.school = school
        self.school_count = school_count
        self.course_name = course_name
        self.section = section
        self.course_state = course_state
        self.guardians_enabled = guardians_enabled
        self.primary_teacher = primary_teacher
        self.primary_teacher_id = primary_teacher_id
        self.primary_teacher_profile = primary_teacher_profile
        self.teacher_count = teacher_count
        self.teachers = teachers
        self.go_to_classroom = go_to_classroom
        self.student_count = student_count
        self.coursework_count = coursework_count
        self.most_recent_coursework = most_recent_coursework
        self.announcement_count = announcement_count
        self.most_recent_announcement = most_recent_announcement
        self.join_code = join_code
        self.last_refresh = last_refresh
        self.most_recent_activity = most_recent_activity
        self.created_at = created_at
        self.updated_at = updated_at
