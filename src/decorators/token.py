import os
from datetime import datetime
from functools import wraps

from flask import request
from google.auth.transport import requests
from google.oauth2 import id_token
from sqlalchemy.orm.exc import NoResultFound

from models import User, db

CLIENT_ID = os.environ.get("GOOGLE_OAUTH_CLIENT_ID")


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token_id = get_token_id()

        if token_id is None:
            return {'message': 'Token is missing!'}, 401

        try:
            id_info = id_token.verify_oauth2_token(token_id, requests.Request(), CLIENT_ID)
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                return {'message': 'Wrong issuer!'}

            # If auth request is from a G Suite domain:
            # gsuite_domain_name = 'seeu.edu.mk'
            # if id_info['hd'] != gsuite_domain_name:
            #     raise ValueError('Wrong hosted domain.')

            # Get user from the id token
            user = get_user(id_info)
            if user is None:
                return {'message': 'You are not a user of this system. Please contact your '
                                   'organization'}, 403

        except ValueError:
            # Invalid token
            return {'message': 'Token was invalid!'}, 401

        return f(user, *args, **kwargs)

    return decorated


def get_token_id():
    token_id = None

    if 'id_token' in request.headers:
        token_id = request.headers['id_token']

    return token_id


def get_user(user_info):
    try:
        email = user_info['email']
        query = User.query.filter_by(email=email)
        user = query.one()
    except NoResultFound:
        return None

    # Update basic user info in case of any changes
    user.first_name = user_info['given_name']
    user.last_name = user_info['family_name']
    user.last_access = datetime.now()
    user.picture_url = user_info['picture']
    db.session.add(user)
    db.session.commit()

    return user
