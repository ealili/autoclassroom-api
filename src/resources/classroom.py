"""
Define the REST verbs relative to the classrooms
"""

from flask.json import jsonify
from flask_restful import Resource

from decorators import token_required
from repositories import ClassroomRepository


class ClassroomResource(Resource):
    """ Verbs relative to the classrooms """

    @staticmethod
    @token_required
    def get(user):
        """ Return all classrooms """
        return jsonify(
            {"classrooms": [classroom.json for classroom in ClassroomRepository.get_all()]})
