"""
Define the REST verbs relative to the users
"""

from flask.json import jsonify
from flask_restful import Resource
from flask_restful.reqparse import Argument
from sqlalchemy.orm.exc import NoResultFound

from decorators import token_required
from models import Invitation, Role, User, db
from repositories import UserRepository
from util import invite_user, parse_params


class UserResource(Resource):
    """ Verbs relative to the users """

    @staticmethod
    @token_required
    def get(user):
        user = UserRepository.get(email=user.email)
        return jsonify({"user": user.json})

    @staticmethod
    @token_required
    @parse_params(
        Argument("email", location="json", required=True, help="The email of the user."),
        Argument("role_name", location="json", required=True, help="The role name of the user.")
    )
    def post(user, email, role_name):
        """ Invite user based on the sent information """

        role = Role.query.filter_by(id=user.role_id).first()
        if role.name != 'Super Admin':
            return {'message': 'Cannot perform that function.'}, 403

        try:
            User.query.filter_by(email=email).one()
        except NoResultFound:
            try:
                Invitation.query.filter_by(email=email).one()
            except NoResultFound:
                invitation = Invitation(email=email, role=role_name)
                db.session.add(invitation)
                db.session.commit()
                invite_user(email, role_name)
                return jsonify({"user": invitation.json})

            return {'message': 'The user with this email has already been invited!'}, 400

        return {'message': 'The user user with this email already exists!'}, 400
    # @staticmethod
    # @parse_params(
    #     Argument("age", location="json", required=True, help="The age of the user.")
    # )
    # @swag_from("../swagger/user/PUT.yml")
    # def put(last_name, first_name, age):
    #     """ Update an user based on the sent information """
    #     repository = UserRepository()
    #     user = repository.update(last_name=last_name, first_name=first_name, age=age)
    #     return jsonify({"user": user.json})
