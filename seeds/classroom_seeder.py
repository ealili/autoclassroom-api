from seeds.base_seeder import BaseSeeder
from models import Classroom
from datetime import datetime


class ClassroomSeeder(BaseSeeder):
    def run(self):
        classrooms = [Classroom("classroom_id", "SEEU", 0, "Game Programming",
                                "[Spring - 19/20]/U/EN/TE",
                                "ACTIVE", False, "Xhemal Zenuni", 0,
                                "teacher-profile", 1, "Xhemal Zenuni",
                                "https://classroom.google.com/c/Mzg1MzUyMzA2NDda", 0, 0,
                                "recent course work",
                                0, "recent announcement", "dxnuc35",
                                datetime.utcnow(), "recent activity",
                                "2020-02-14T15:48:45.212Z", "2020-02-14T15:47:00.421Z"),
                      Classroom("classroom_id", "SEEU", 0, "Capstone Project",
                                "[Spring - 19/20]/U/EN/TE",
                                "ACTIVE", False, "Besnik Selimi", 0,
                                "teacher-profile", 1, "Besnik Selimi",
                                "https://classroom.google.com/c/MjQyNTIxNjg4MTNa", 0, 0,
                                "recent course work",
                                0, "recent announcement", "zvvjmrc",
                                datetime.utcnow(), "recent activity",
                                "2020-02-14T15:47:00.421Z", "2020-04-13T07:09:22.120Z"),
                      Classroom("classroom_id", "SEEU", 0, "Python Programming",
                                "[Spring - 19/20]/U/EN/TE",
                                "ACTIVE", False, "Besnik Selimi", 0,
                                "teacher-profile", 1, "Besnik Selimi",
                                "https://classroom.google.com/c/MjQyNTI2NjYzNTda", 0, 0,
                                "recent course work",
                                0, "recent announcement", "z3j3m7n",
                                datetime.utcnow(), "recent activity",
                                "2020-02-14T15:48:17.996Z", "2020-04-13T07:08:48.416Z")]

        for classroom in classrooms:
            self.db.session.add(classroom)
