from flask import Blueprint
from flask_restful import Api

from resources import InsightsResource

INSIGHTS_BLUEPRINT = Blueprint('insights', __name__)
Api(INSIGHTS_BLUEPRINT).add_resource(
    InsightsResource, "/insights"
)
