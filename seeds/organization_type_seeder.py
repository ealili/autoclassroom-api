from seeds.base_seeder import BaseSeeder
from models import OrganizationType


class OrganizationTypeSeeder(BaseSeeder):
    def run(self):
        ot = OrganizationType('Student')
        ot1 = OrganizationType('Teacher')

        self.db.session.add(ot)
        self.db.session.add(ot1)
