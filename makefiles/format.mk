### FORMAT
# ¯¯¯¯¯¯¯¯

format.flake8: ## Run black on every file
	docker-compose run --rm server bash -c "python -m flake8"

format.isort: ## Sort imports
	docker-compose run --rm server bash -c "python vendor/bin/isort -rc src/ test/ --skip vendor/ --skip src/models/__init__.py"
