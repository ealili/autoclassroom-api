"""
Defines the blueprint for the roles
"""

from flask import Blueprint, request
from flask.json import jsonify
from flask_restful import Api

from models import Settings
from resources import SettingsResource

SETTINGS_BLUEPRINT = Blueprint("settings", __name__)
Api(SETTINGS_BLUEPRINT).add_resource(
    SettingsResource, "/settings/<int:_id>"
)


ALLSETTINGS_BLUEPRINT = Blueprint("allsettings", __name__)


@ALLSETTINGS_BLUEPRINT.route('/settings/', methods=['GET', 'POST'])
def post():
    """ Create new settings"""
    if request.method == 'POST':
        api_key = request.form['api_key']
        designated_admin = request.form['designated_admin']
        domain = request.form['domain']
        gsuite_id = request.form['gsuite_id']
        organization_name = request.form['organization_name']
        settings = Settings(api_key=api_key, designated_admin=designated_admin, domain=domain,
                            gsuite_id=gsuite_id, organization_name=organization_name)
        settings.save()

        return jsonify({"settings": settings.json})

    """ Return all settings """
    if request.method == 'GET':
        allSettings = Settings.query.all()
        data = []
        for settings in allSettings:
            data.append(settings.json)

        return jsonify({"settings": data})

    return 'smth is wrong'
