"""
Define the REST verbs relative to the insights
"""

from flask.json import jsonify
from flask_restful import Resource

from decorators import token_required
from models import Classroom


class InsightsResource(Resource):
    """ Verbs relative to the users """

    @staticmethod
    @token_required
    def get(user):
        number_of_classes = Classroom.query.count()
        number_of_active_classes = Classroom.query.filter_by(course_state='ACTIVE').count()
        number_of_archived_classes = Classroom.query.filter_by(course_state='ARCHIVED').count()
        number_of_declined_classes = Classroom.query.filter_by(course_state='DECLINED').count()
        number_of_provisioned_classes = Classroom.query.filter_by(
            course_state='PROVISIONED').count()

        insights = {
            'number_of_classes': number_of_classes,
            'number_of_active_classes': number_of_active_classes,
            'number_of_archived_classes': number_of_archived_classes,
            'number_of_declined_classes': number_of_declined_classes,
            'number_of_provisioned_classes': number_of_provisioned_classes
        }
        return jsonify({'insights': insights})
