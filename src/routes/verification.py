"""
Defines the blueprint for verification
"""
from flask import Blueprint
from flask_restful import Api

from resources import VerificationResource

VERIFICATION_BLUEPRINT = Blueprint('verify', __name__)
Api(VERIFICATION_BLUEPRINT).add_resource(VerificationResource, '/token')
