"""
Define the Teacher model
"""

from . import db
from .abc import BaseModel, MetaBaseModel


class Teacher(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Teacher model """

    __tablename__ = "teacher"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    username = db.Column(db.String(300), nullable=False)
    email = db.Column(db.String(300), nullable=False)

    def __init__(self, name, username, email):
        """ Create a new Teacher """
        self.name = name
        self.username = username
        self.email = email
