import smtplib
from email.message import EmailMessage


def invite_user(email, role):
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()

        smtp.login('autoclassroom2020@gmail.com', 'kamsseinfcbhmbpe')
        sender = 'autoclassroom2020@gmail.com'
        receiver = email

        # construct email
        email = EmailMessage()
        email['Subject'] = 'Invitation'
        email['From'] = sender
        email['To'] = receiver
        content = f"""<h2>You have been granted access to AutoClassroom as {role}.</h2>
        <h3>Please login to AutoClassroom to accept your invitation.</h3>
        <a href="http://localhost:3001"><button style="color: #fff; background-color: #28a745; 
        border-color: #28a745; padding: .375rem .75rem;font-size: 1rem;font-weight: 400;
        text-align: center;">Accept Invitation</button></a></div>"""

        email.set_content(content, subtype='html')
        smtp.send_message(email)
