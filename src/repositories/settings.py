""" Defines the Settings repository """
from models import Settings


class SettingsRepository:
    """ The repository for the settings model """

    def __init__(self):
        pass

    @staticmethod
    def get(id):
        """ Query a user by its id """
        settings = Settings.query.filter_by(id=id).one()
        return settings

    def update(self, id, api_key, designated_admin, organization_name,):
        """ Update a user's organization name """
        settings = self.get(id)
        settings.api_key = api_key
        settings.designated_admin = designated_admin
        settings.organization_name = organization_name

        return settings.save()

    @staticmethod
    def create(api_key, designated_admin, domain, gsuite_id, organization_name):
        """ Create a new settings """
        settings = Settings(api_key=api_key, designated_admin=designated_admin, domain=domain,
                            gsuite_id=gsuite_id, organization_name=organization_name)

        return settings.save()
