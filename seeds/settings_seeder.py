from seeds.base_seeder import BaseSeeder

from models import Settings, User


class SettingsSeeder(BaseSeeder):
    def run(self):
        settings = [Settings('Wilson School', 'wilsonschool.edu.mk',
                             'C00mbjwoi', User.query.filter_by(first_name='Selaudin').first().id,
                             'bb5142fc-becf-4d93-97fe-458d397b88b1'),
                    Settings('Wilson School', 'wilsonschool.edu.mk',
                             'C00mbjwoi', User.query.filter_by(first_name='Selaudin').first().id,
                             'bb5142fc-becf-4d93-97fe-458d397b88b2'),
                    Settings('Wilson School', 'wilsonschool.edu.mk',
                             'C00mbjwoi', User.query.filter_by(first_name='Selaudin').first().id,
                             'bb5142fc-becf-4d93-97fe-458d397b88b3'),
                    Settings('Wilson School', 'wilsonschool.edu.mk',
                             'C00mbjwoi', User.query.filter_by(first_name='Selaudin').first().id,
                             'bb5142fc-becf-4d93-97fe-458d397b88b4'),
                    Settings('Wilson School', 'wilsonschool.edu.mk',
                             'C00mbjwoi', User.query.filter_by(first_name='Selaudin').first().id,
                             'bb5142fc-becf-4d93-97fe-458d397b88b5')]

        for setting in settings:
            self.db.session.add(setting)
