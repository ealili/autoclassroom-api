from seeds.classroom_seeder import ClassroomSeeder
from seeds.role_seeder import RoleSeeder
from seeds.settings_seeder import SettingsSeeder
from seeds.user_seeder import UserSeeder
from seeds.organization_type_seeder import OrganizationTypeSeeder
from flask_seeder import Seeder


class Seeder(Seeder):
    # run() will be called by Flask-Seeder
    def run(self):
        classroom_seeder = ClassroomSeeder(self.db)
        classroom_seeder.run()

        role_seeder = RoleSeeder(self.db)
        role_seeder.run()

        user_seeder = UserSeeder(self.db)
        user_seeder.run()

        settings_seeder = SettingsSeeder(self.db)
        settings_seeder.run()

        organization_type_seeder = OrganizationTypeSeeder(self.db)
        organization_type_seeder.run()
