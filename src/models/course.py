"""
Define the Course model
"""

from . import db
from .abc import BaseModel, MetaBaseModel


class Course(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Course model """

    __tablename__ = "course"

    id = db.Column(db.String(50), primary_key=True)
    title = db.Column(db.String(300), nullable=False)

    def __init__(self, id, title):
        """ Create a new Course """
        self.id = id
        self.title = title
