"""
Define the REST verbs relative to the verification
"""
from flask_restful import Resource
from sqlalchemy.orm.exc import NoResultFound

from decorators import token_required
from models import Invitation, Role
from repositories import UserRepository


class VerificationResource(Resource):

    @staticmethod
    @token_required
    def post(user):
        try:
            invitation = Invitation.query.filter_by(email=user.email).one()
            role_id = Role.query.filter_by(name=invitation.role).first().id
            UserRepository.create(
                first_name=user.first_name, last_name=user.last_name, email=user.email,
                role_id=role_id, suspended=None, last_access=None, picture_url=None
            )
            invitation.delete()
        except NoResultFound:
            pass

        return {'message': 'Token has been verified!'}
