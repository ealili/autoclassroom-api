"""
Define the Teacher Organization Unit model
"""
import datetime

from sqlalchemy import ForeignKey

from . import db
from .abc import BaseModel, MetaBaseModel


class TeacherOrganizationUnit(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Teacher Organization Unit model """

    __tablename__ = "teacher_organization_unit"

    id = db.Column(db.Integer, primary_key=True)
    organization_unit_id = db.Column(db.Integer, ForeignKey('organization_unit.id'))
    teacher_id = db.Column(db.Integer, ForeignKey('teacher.id'))
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    updated_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())

    def __init__(self, organization_unit_id, teacher_id, created_at, updated_at):
        """ Create a new Teacher Organization Unit """
        self.organization_unit_id = organization_unit_id
        self.teacher_id = teacher_id
        self.created_at = created_at
        self.updated_at = updated_at
