"""
Define the Organization Type model
"""

from . import db
from .abc import BaseModel, MetaBaseModel


class OrganizationType(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Organization Type model """

    __tablename__ = "organization_type"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)

    def __init__(self, name):
        """ Create a new Organization Type """
        self.name = name
