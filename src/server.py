#! /usr/local/lib/python
import os

from flasgger import Swagger
from flask import Flask, jsonify
from flask.blueprints import Blueprint
from flask_cors import CORS
from flask_seeder import FlaskSeeder

import config
import routes
from models import db

# config your API specs
# you can define multiple specs in the case your api has multiple versions
# omit configs to get the default (all views exposed in /spec url)
# rule_filter is a callable that receives "Rule" object and
# returns a boolean to filter in only desired views

SERVER_IP = os.environ.get("SERVER_IP")
SERVER_DOMAIN = os.environ.get("SERVER_DOMAIN")
LOCALHOST = os.environ.get("LOCALHOST")

server = Flask(__name__)
CORS(server, resources={r"/api/*": {"origins": [SERVER_IP, SERVER_DOMAIN, LOCALHOST]}})

server.config["SWAGGER"] = {
    "swagger_version": "2.0",
    "title": "Application",
    "specs": [
        {
            "version": "0.0.1",
            "title": "Application",
            "endpoint": "spec",
            "route": "/application/spec",
            "rule_filter": lambda rule: True,  # all in
        }
    ],
    "static_url_path": "/apidocs",
}

Swagger(server)

server.debug = config.DEBUG
server.config["SQLALCHEMY_DATABASE_URI"] = config.DB_URI
server.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = config.SQLALCHEMY_TRACK_MODIFICATIONS
server.secret_key = os.getenv("FLASK_SECRET_KEY")

db.init_app(server)
db.app = server

seeder = FlaskSeeder()
seeder.init_app(server, db)


@server.errorhandler(404)
def page_not_found(error):
    return jsonify({'message': str(error)}), 404


for blueprint in vars(routes).values():
    if isinstance(blueprint, Blueprint):
        server.register_blueprint(blueprint, url_prefix=config.APPLICATION_ROOT)

if __name__ == "__main__":
    server.run(host=config.HOST, port=config.PORT)
