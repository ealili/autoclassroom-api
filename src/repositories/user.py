""" Defines the User repository """

from models import User


class UserRepository:
    """ The repository for the user model """

    @staticmethod
    def get(*, email):
        """ Query a user by email """
        # Needs to be joined with the Role table
        return User.query.filter_by(email=email).first()

    @staticmethod
    def get_all():
        """ Query all users"""
        return User.query.all()

    # def update(self, last_name, first_name, age):
    #     """ Update a user's age """
    #     user = self.get(last_name, first_name)
    #     user.age = age
    #
    #     return user.save()
    #
    #

    @staticmethod
    def create(first_name, last_name, email, role_id,
               suspended, last_access, picture_url):
        """ Create a new user """
        user = User(first_name=first_name, last_name=last_name, email=email, role_id=role_id,
                    suspended=suspended, last_access=last_access, picture_url=picture_url)

        return user.save()
