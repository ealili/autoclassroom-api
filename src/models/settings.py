"""
Define the Settings model
"""
from sqlalchemy import ForeignKey

from . import db
from .abc import BaseModel, MetaBaseModel


class Settings(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Settings model """

    __tablename__ = "settings"
    id = db.Column(db.Integer, primary_key=True)
    organization_name = db.Column(db.String(300), nullable=False)
    domain = db.Column(db.String(300), nullable=False)
    gsuite_id = db.Column(db.String(300), nullable=False)
    designated_admin = db.Column(db.Integer, ForeignKey('user.id'))
    api_key = db.Column(db.String(300), nullable=False)

    def __init__(self, organization_name, domain, gsuite_id, designated_admin, api_key):
        """ Create a new Settings """
        self.organization_name = organization_name
        self.domain = domain
        self.gsuite_id = gsuite_id
        self.designated_admin = designated_admin
        self.api_key = api_key
