"""
Define the REST verbs relative to the users
"""

from flask.json import jsonify
from flask_restful import Resource

from decorators import token_required
from models import Role
from repositories import UserRepository


class UsersResource(Resource):
    """ Verbs relative to the users """

    @staticmethod
    @token_required
    def get(user):
        """ Return all users """
        user_role = Role.query.filter_by(id=user.role_id).first()
        if user_role.name != 'Super Admin':
            return jsonify({'message': 'You do not have enough permissions to access this '
                                       'resource'}), 403
        users = [user.json for user in UserRepository.get_all()]
        return jsonify({"users": users})
