"""
Define the Invitation model
"""

from . import db
from .abc import BaseModel, MetaBaseModel


class Invitation(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Invitation model """

    __tablename__ = "invitation"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(300), nullable=False)
    role = db.Column(db.String(300), nullable=False)

    def __init__(self, email, role):
        """ Create a new invitation """
        self.email = email
        self.role = role
