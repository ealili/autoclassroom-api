""" Defines the Role repository """
from models import Role


class RoleRepository:
    """ The repository for the role model """

    def __init__(self):
        pass

    @staticmethod
    def get_all():
        roles = Role.query.all()
        return roles
