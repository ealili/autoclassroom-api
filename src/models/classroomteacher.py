"""
Define the ClassroomTeacher model
"""
import datetime

from sqlalchemy import ForeignKey

from . import db
from .abc import BaseModel, MetaBaseModel


class ClassroomTeacher(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The ClassroomTeacher model """

    __tablename__ = "classroom_teacher"

    id = db.Column(db.Integer, primary_key=True)
    classroom_id = db.Column(db.Integer, ForeignKey('classroom.id'))
    teacher_id = db.Column(db.Integer, ForeignKey('teacher.id'))
    is_primary = db.Column(db.Boolean)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    updated_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())

    def __init__(self, classroom_id, teacher_id, is_primary, created_at, updated_at):
        """ Create a new ClassroomTeacher """
        self.classroom_id = classroom_id
        self.teacher_id = teacher_id
        self.is_primary = is_primary
        self.created_at = created_at
        self.updated_at = updated_at
