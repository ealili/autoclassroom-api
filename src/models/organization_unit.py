"""
Define the Organization Unit model
"""

from sqlalchemy import ForeignKey

from . import db
from .abc import BaseModel, MetaBaseModel


class OrganizationUnit(db.Model, BaseModel, metaclass=MetaBaseModel):
    """ The Organization Unit model """

    __tablename__ = "organization_unit"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    organization_type_id = db.Column(db.Integer, ForeignKey('organization_type.id'))

    def __init__(self, name, organization_type_id):
        """ Create a new Organization Unit """
        self.name = name
        self.organization_type_id = organization_type_id
