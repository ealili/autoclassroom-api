"""
Define the REST verbs relative to the csv
"""

from datetime import datetime

import pandas as pd
import requests
from flask import jsonify, request
from flask_restful import Resource

from decorators import token_required
from models import Classroom, Role


def process_file(file, access_token):
    """ Add classes to google classroom from csv file"""
    data = pd.read_csv(file)
    dct = data.to_dict(orient='records')
    for row in dct:
        course = {
            "name": row["Course Title"],
            # section is generated like Semester Code/Degree Level/Language/Location
            "section": f"[{row['Semester Code']}]/{row['Degree Level']}/{row['Language']}"
                       f"/{row['Location']}",
            "ownerId": "me"
        }
        try:
            result = requests.post('https://classroom.googleapis.com/v1/courses',
                                   json=course,
                                   headers={'Content-Type': 'application/json',
                                            'Authorization': 'Bearer {}'.format(access_token)})
            result.raise_for_status()

            result = result.json()

            primary_teacher = row['Teacher Email']  # local
            primary_teacher_id = 1  # query by email local
            primary_teacher_profile = row['Teacher Email']
            teacher_count = 1  # GET google
            teachers = row['Teacher Email']  # local REMOVE

            classroom = Classroom(
                classroom_id=result['id'],
                school=row['School ID'],
                school_count=0,
                course_name=result['name'],
                section=result['section'],
                course_state=result['courseState'],
                guardians_enabled=result['guardiansEnabled'],
                primary_teacher=primary_teacher,
                primary_teacher_id=primary_teacher_id,
                primary_teacher_profile=primary_teacher_profile,
                teacher_count=teacher_count,
                teachers=teachers,
                go_to_classroom=result['alternateLink'],
                student_count=0,
                coursework_count=0,
                most_recent_coursework=None,
                announcement_count=0,
                most_recent_announcement=None,
                join_code=result['enrollmentCode'],
                last_refresh=datetime.utcnow(),
                most_recent_activity=None,
                created_at=result['creationTime'],
                updated_at=result['updateTime'])
            classroom.save()

        except requests.exceptions.RequestException as err:
            return {'message': str(err)}


class CSVResource(Resource):
    """ Verbs relative to the csv """

    @staticmethod
    @token_required
    def post(user):
        if user.role_id != Role.query.filter_by(name='Super Admin').first().id:
            return {'message': 'You do not have permissions to create classrooms!'}, 403

        if 'access_token' not in request.headers:
            return {'message': 'Access token is missing!'}

        access_token = request.headers['access_token']

        resp = requests.get(f'https://www.googleapis.com/oauth2/v3/tokeninfo?access_token'
                            f'={access_token}')

        if not resp.ok:
            return {'message': 'Invalid access token!'}, 401

        if 'file' not in request.files:
            return jsonify({'message': 'No file was attached'})

        file = request.files['file']
        # Process csv file to add classes to google classroom
        process_file(file, access_token)

        return {'message': 'Courses have been successfully created!'}
