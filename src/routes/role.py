"""
Defines the blueprint for the roles
"""
from flask import Blueprint
from flask_restful import Api

from resources import RoleResource

ROLE_BLUEPRINT = Blueprint("role", __name__)
Api(ROLE_BLUEPRINT).add_resource(
    RoleResource, "/roles"
)
